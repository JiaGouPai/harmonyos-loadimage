package com.jiagoupai.client;

import com.jiagoupai.client.slice.HomeSlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class HomeAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(HomeSlice.class.getName());
    }
}
