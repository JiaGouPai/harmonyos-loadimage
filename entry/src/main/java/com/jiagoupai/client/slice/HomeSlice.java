package com.jiagoupai.client.slice;

import com.jiagoupai.client.ResourceTable;
import com.jiagoupai.client.utils.ImageReadUtil;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.TaskPriority;
import ohos.media.image.PixelMap;

public class HomeSlice extends AbilitySlice {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initBtnListener();
    }

    private void initBtnListener() {

        Button btnSearch = (Button) findComponentById(ResourceTable.Id_btn_search);

        btnSearch.setClickedListener(component ->  {
            //示例图片url,
            Text imageUrlText = (Text) findComponentById(ResourceTable.Id_url_image_example);

            //为了模拟手动输入url，此处将直接获取imageUrlText的url链接填充到输入框中
            TextField textField = (TextField) findComponentById(ResourceTable.Id_input_text_search);
            textField.setText(imageUrlText.getText());

            /**
             * 读取网络数据的耗时操作在TaskDispatcher分发的单独线程中处理，
             * 否则会报 "NetworkOnMainThreadException"异常
             */
            TaskDispatcher refreshUITask = createParallelTaskDispatcher("", TaskPriority.DEFAULT);
            refreshUITask.syncDispatch(()->{
                String inputUrl = textField.getText();
                PixelMap pixelMap = ImageReadUtil.createPixelMap(inputUrl);

                //Image组件填充位图数据，ui界面更新
                Image image = (Image) findComponentById(ResourceTable.Id_net_image_example);
                image.setPixelMap(pixelMap);
                pixelMap.release();
            });
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
